#Decimation
-----

Ce programme permet de créer N niveaux de détails discrets à partir d’un fichier obj.
on utilise l’algorithme de la librairie openmesh basé sur l’algorithme de décimation error quadrics de garland et heckbert pour décimer le maillage d’entrée jusqu’à un niveau de détail donné. le niveau de décimation est un pourcentage donné du nombre de vertices du maillage d’entrée.
en sortie sont crées les n fichiers contenant respectivement les maillages des niveaux de détails générés.

Ma config : OSX 10.9, clang 6.0, OpenMesh (v3.2), Qt5.3.1, NodeJS v0.10.32

    clang -v
    Apple LLVM version 6.0 (clang-600.0.51) (based on LLVM 3.5svn)
    Target: x86_64-apple-darwin13.4.0
    Thread model: posix

##C++ part
-----

1. Installer Qt

2. Installer la bibliothèque OpenMesh et ses dépendances
  - Download : http://www.openmesh.org/download/
  - Compilation : http://www.openmesh.org/media/Documentations/OpenMesh-Doc-Latest/a00030.html
Installer la version ```release``` et vérifier qu'elle se trouve dans ```/usr/local/lib/OpenMesh``` et les includes dans ```/usr/local/include```
(si les paramètres sont différents, modifier les chemins dans le fichier  le dossier ```lib/OpenMeshDecimater/OpenMeshDecimater.pro``` - avec c'est plus simple QtCreator et recompiler dans le dossier ```lib/build```)

3. Compilation 
        cd lib/build
        qmake ../OpenMeshDecimater/OpenMeshDecimater.pro
        make
    
    *Qui donne ...*
    
        ls 
        Makefile          OpenMeshDecimater lod.o             main.o
    
----
###Utilisation

        usage: ./Users/caroline/workspace/docdoku/lib/build/OpenMeshDecimater -i <inputFile> -o <outputPath> [OPTIONS]
        usage: and <outputPath> must exist before execution of the program

        OPTIONS (choose one among these):
        	<N> ...  : Describe discrete LOD : Where 0.0 < N < 1.0 decimate input mesh down to N%.
        		Example : ./Users/caroline/workspace/docdoku/lib/build/OpenMeshDecimater -i <inputFile> -o <outputPath> 0.1 0.02 0.5 
         	-e <nbLOD>  : Generate LOD with exponential function : Where <nbLOD> > 1.
        		Example : ./Users/caroline/workspace/docdoku/lib/build/OpenMeshDecimater -i <inputFile> -o <outputPath> -e 5 
         	-q <nbLOD>  : Generate LOD with quadratic function : Where <nbLOD> > 1.
        		Example : ./Users/caroline/workspace/docdoku/lib/build/OpenMeshDecimater -i <inputFile> -o <outputPath> -q 3 
    
    
###Résultats

Résultats du temps d’exécution avec le dragon.obj contenant 435545 vertex sur 3 LODs en utilisant le programme ```/usr/bin/time```

* Single thread vs multithread - Génération manuelle de 3 LOD : 
    * Single thread 

            ./OpenMeshDecimater -i dragon.obj -o . 0.2 0.4 0.6
            101.68 real        99.46 user         2.13 sys

    * Multithread

            ./OpenMeshDecimater -i dragon.obj -o . 0.2 0.4 0.6
            41.61 real       108.66 user         2.22 sys

L’interet principal du multithread est de réduire le temps d’execution (divisé par 2+ sur l’exemple)

* Génération automatique de 5 LOD (multithread)
     * Quadratique

        ![quadratique.png](https://bitbucket.org/repo/XMqMMe/images/2668209418-quadratique.png)

            ./OpenMeshDecimater -i ../../data/dragon.obj -o . -q 5
            % 0.972222
            % 0.888889
            % 0.75
            % 0.555556
            % 0.305556
            18.50 real        40.78 user         6.15 sys


    * Exponentielle

        ![exponentielle.png](https://bitbucket.org/repo/XMqMMe/images/1485316471-exponentielle.png)

            ./OpenMeshDecimater -i ../../data/dragon.obj -o . -e 5
            % 0.894839
            % 0.64118
            % 0.367879
            % 0.169013
            % 0.0621765
            20.92 real        54.72 user         3.17 sys

----------
NodeJS part 
---------
Création et visualisation interactive des LOD générés avec ThreeJS
```
git clone git clone git@bitbucket.org:caro3801/docdoku.git decimation
cd decimation
npm install
```
Les fichiers ```.obj``` sont dans le dossier ```data```
Les fichiers créés sont dans le fichier ```javascript/files```

Les niveaux de détails sont entrés en dur dans ```routes/loadModel.js```
Actuellement c'est la version "génération automatique quadratique" qui est utilisée à la ligne 62
```
62. var commande="../lib/build/OpenMeshDecimater -i "+filePath+" -o "+dirPath+" -q "+sLOD().length;
...
...
66. //percent of LOD
67. var decimPercent = [0.02,0.3,0.04,0.8];
```