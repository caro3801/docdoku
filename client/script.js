/**
 * Created by caroline on 5/5/14.
 */
var THREE=require ('three');
var Trackball = require('three.trackball');
require('../lib/OBJLoader');

var XHR = require ("./XHR");

var clock = new THREE.Clock();
var scene, camera,controls, renderer;
var geometry, material, mesh;
var lod = new THREE.LOD();

document.addEventListener("DOMContentLoaded", function() {

    initDOM();
});

init();
animate();


function initDOM(){

    document.querySelector("form").addEventListener('submit',getLODs,false);

}
function init() {

    camera = new THREE.PerspectiveCamera( 45, 1000 / 1000, 0.1, 100000 );
    camera.position.z = 1000;
    controls = new Trackball( camera );

    controls.rotateSpeed = 3.0;
    controls.zoomSpeed = 1;
    controls.panSpeed = 0.8;

    controls.noZoom = false;
    controls.noPan = false;

    controls.staticMoving = true;
    controls.dynamicDampingFactor = 0.3;

    controls.keys = [ 65, 83, 68 ];

    controls.addEventListener( 'change', render );

    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer();

    renderer.sortObjects = false;
    renderer.setSize( 1000, 1000 );

    document.getElementById("container").appendChild( renderer.domElement );

}
function getLODs(event){
    //pseudo clear scene
    if(scene.children[0]){
        scene.remove(scene.children[0]);
    }
    lod = new THREE.LOD();
    var xhr = new XHR(XHR.createXMLHttpRequest());
    xhr.postMP("/model/upload", false);
    var formD = new FormData(event.target);

    xhr.send(formD);

    event.preventDefault();
    var res=JSON.parse(xhr.jsonText);
    //loadModel(res.dirPath+res.LOD[0]);
    loadLODs(res);
}

function getObjModel(url, callback){

    var loader = new THREE.OBJLoader();
    if (url){

        loader.load(url,callback);
    }
}

function generateCallBack(distance) {
    return function(object) {
        var mesh = object.children[0];
        mesh.material=material;
        mesh.geometry.computeVertexNormals(true);
        mesh.geometry.computeFaceNormals(true);
        mesh.geometry.computeVertexNormals(true);

        //ajust scaling
        mesh.geometry.computeBoundingBox();
        var bb=mesh.geometry.boundingBox;
        var diag = new THREE.Vector3();
        diag = diag.sub(bb.max, bb.min);
        var radius = diag.length() * 0.5;
        var offset = radius / Math.tan(Math.PI / 180.0 * camera.fov * 0.5);
        mesh.scale.set( (1/offset)*800,(1/offset)*800,(1/offset)*800);


        mesh.updateMatrix();
        mesh.matrixAutoUpdate = false;


        lod.addLevel(mesh, distance);

    }
}

function loadLODs(paths){
    var j= 20;
    var i=0;
    material = new THREE.MeshBasicMaterial( { color: 0xffff00, wireframe: true } );

    var distances = [200,500,800,1000,2000];
    console.log(distances);
    for (i;i<paths.LOD.length;i++){
        //lod.addLevel(new THREE.Mesh(new THREE.IcosahedronGeometry( 100, paths.LOD.length-i ), material),distances[i]);
        getObjModel(paths.dirPath+paths.LOD[i], generateCallBack(distances[i]));
    }

    scene.add(lod);
}


function animate() {

    controls.update(clock.getDelta());
    requestAnimationFrame( animate );
    render();


}
var oldDistance = 0;
function render(){

    scene.updateMatrixWorld();
    scene.traverse( function ( object ) {

        if ( object instanceof THREE.LOD ) {
            var v1 = new THREE.Vector3();
            var v2 = new THREE.Vector3();
            v1.setFromMatrixPosition( camera.matrixWorld );
            v2.setFromMatrixPosition( lod.matrixWorld );
            var distance = v1.distanceTo( v2 );
            if(oldDistance != distance){
                oldDistance = distance;
                console.log(distance);
            }
            object.update( camera );

        }

    } );

    renderer.render(scene, camera);
}

module.exports ;