var express = require('express');
var router = express.Router();
var fs = require("fs");
var sys = require("sys");
var exec = require('child_process').exec;

exports.upload = function(req,res){
    var file = req.body.model;
    var fstream;
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filenameExt) {

        //get the uploaded file in a directory named after the file
        var filename=filenameExt.replace(/\.[^/.]+$/,"");
        var dirPath=__dirname + '/../public/files/' +filename +"/";
        if(!fs.existsSync(dirPath)){
            fs.mkdirSync(dirPath, 0766, function(err){
                if(err){
                    console.log(err);
                    response.send("ERROR! Can't make the directory! \n");    // echo the result back
                }
            });
        }

        var filePath = dirPath + filenameExt;
        console.log("Uploading: " + filenameExt);
        fstream = fs.createWriteStream(filePath);
        file.pipe(fstream);
        fstream.on('close', function () {

            console.log("File loaded ... ");
        });


        function puts(error, stdout, stderr) {
            sys.puts(stdout);
            decimPercent.sort(function(a,b){
                return a<b;
            });
            var LODNames=[filenameExt];
            for (var val in decimPercent){
                LODNames.push(filename+decimPercent[val]*100+".obj");
            }

            var response = {dirPath:"files/"+filename+"/",
                LOD:LODNames};

            console.log("finished...");
            res.send(response);
        }


        //generate command line
        function cmdlDOM(filePath,dirPath,LODs){
            var sLOD = function(){
                var s="";
                for(var l in LODs){
                    s+=" "+LODs[l];
                }
                return s;
            };
            var commande="../lib/build/OpenMeshDecimater -i "+filePath+" -o "+dirPath+" -q "+sLOD().length;
            return commande;
        }

        //percent of LOD
        var decimPercent = [0.02,0.3,0.04,0.8];

        exec(cmdlDOM(filePath,dirPath,decimPercent),puts);




    });


};
